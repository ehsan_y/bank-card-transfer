<?php

namespace App\Listeners;

use App\Events\TransferCommited;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Messages\transferMessageBuilder;
use App\SMS\SmsFactory;

class SendDepositNotification
{
    use transferMessageBuilder;
    use SmsFactory;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\TransferCommited  $event
     * @return void
     */
    public function handle(TransferCommited $event)
    {
        $transferDetails = $event['transferDetails'];
        $destinationUser = $event['destinationUser'];

        //Message Content
        //from transferMessageBuilder
        $this->__set('recieverCard', $transferDetails->destination);
        $this->__set('senderCard', $transferDetails->origin);
        $this->__set('messageType', 'deposit');

        //SMS service
        //from SmsFactory;
        $smsService = $this->createSMSservice();
        $smsService->send($this->message('withdraw'), [$destinationUser->mobile]);
        
    
    }
}
