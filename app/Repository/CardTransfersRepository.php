<?php

namespace App\Repository;

//use App\Repository\CardTransfersRepositoryInterface as TransfersRepoInterface;
use Illuminate\Support\Facades\DB;
use App\Models\Cars;
use App\Models\Trasactions;
use App\Models\CardNumber;
use Config;

class CardTransfersRepository //implements TransfersRepoInterface 
{

    protected $origin;
    protected $destination;
    protected $amount;
    protected $bank = 'mellat';
    protected $card;

    protected function createTransfers(array $transferDetails) 
    {
        DB::beginTransaction();
         $originCard = CardNumber::where('cardNumber', '=', $origin)->get();
         $originCard->balance = $originCard->balance -  $transferDetails->amount;
         $originCard->save();


         $destinCard = CardNumber::where('cardNumber', '=', $destination)->get();
         $destinCard->balance = $originCard->balance +  $transferDetails->amount;
         $originCard->save();

         
         $transaction = Trasactions::create($transferDetails);

         
         TransactionsFees::create([
            'transaction_id' => $transaction->id,
            'fee'=> Config('services.transaction-fee'),
         ]);

         DB::commit();
    }
    protected function verifyMinimumCredit($origin, $amount) {
        $card = CardNumber::where('cardNumber', '=', $origin)->get();
        $card = $card[0];
        if($card->balance > $amount + Config('services.transaction-fee')) {
            return true;
        }
        return false;
    }
    protected function findUserByCardNumber($number) {
        $cardNumber = CardNumber::where('cardNumber', '=', $number)->get();
        $cardNumber = $cardNumber[0];
        
        //This is From Laravel Molde Relations
        return $user = $originCardNumber->accountUser();
    }
  
}