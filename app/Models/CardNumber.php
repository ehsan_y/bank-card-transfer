<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\BankAccount;

class CardNumber extends Model
{
    use HasFactory;

    protected $table = 'cardNumbers';

    public function banckAccount(): BelongsTo 
    {
        return $this->belongsTo(BankAccount::class);
    }

    public function accountUser(): HasOneThrough 
    {
        return $this->hasOneThrough(User::class, BankAccount::class);
    }
}
