<?php

namespace App\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CardNumber;

class TransferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'recieverCard' => ['required', 'string', new CardNumber],
            'senderCard' => ['required', 'string', new CardNumber],
            'amount' => ['required', 'numeric', 'min:1000', 'max:50000000']
        ];
    }
}
