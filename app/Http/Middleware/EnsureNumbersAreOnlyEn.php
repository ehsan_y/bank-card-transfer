<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EnsureNumbersAreOnlyEn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        function convertPersianToEnglish($string) {
            /*this converts any arabic or persian
             number present in card number to an english number, 
             but to make sure that card number is only a numeric value
             without any none numeric characters
             we will check it in the validation layer 
             by numeric rule of validation */

            $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
            $arabic = ['۰', '۱', '۲', '۳', '٤', '۵', '٦', '۷', '۸', '۹'];
            $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
            $request->merge([
                
            'senderCard' => str_replace($persian, $english, $request->senderCard),
            'senderCard' =>  str_replace($arabic, $english, $request->senderCard),
            'senderCard' => str_replace($persian, $english, $request->recieverCard),
            'senderCard' =>  str_replace($arabic, $english, $request->recieverCard),
            
        ]);
        return $next($request);
        }
    }
}
