<?php

namespace App\Http\Controllers;

use Illuminate\Http\Requests;
use App\Repository\CardTransfersRepository;
use App\Models\CardNumber;
use App\Events\TransferCommited;
use App\Requests\TransferRequest;

class Transfer extends Controller
{
    public function tranfer(TransferRequest $transferRequest, CardTransfersRepository $repository) {
        
        $result = $repository->verifyMinimumCredit($transferRequest->origin, $transferRequest->amount);
        if(!$result) {
            return response()
                ->status(401)
                ->json([
                    'message' => 'your card balance is not enough for this opperation', 
                    'status' => 'failed'
                ]);    
        }

        $transferDetails = [
            'originCardNumber' => $transferRequest->origin,
            'destinationCardNumber'  => $transferRequest->destination,
            'amount' => $transferRequest->amount,
        ];
        
        $repository->createTransfers($transferDetails);

        //originUser
        $originUser = $repository->findUserByCardNumber($transferRequest->origin);
        //destinationUser
        $destinationUser = $repository->findUserByCardNumber($transferRequest->destination);
        $event = [
            'destinationUser' => $destinationUser,
            'originUser' => $originUser,
            'transferDetails' => $transferDetails,
        ];
        TransferCommited::dispatch($event);
    }

}
