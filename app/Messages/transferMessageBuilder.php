<?php

namespace App\Messages;


trait transferMessageBuilder {

            use config;

            /*It's also possible to use it in the 
             object context: public $data = Object; */
            private $data = array();

            public function message($type){
               if($data['type'] = 'deposit') {
                        return $recieverMessage=<<<EOD
                        'کاربر گرامی مبلغ مبلغ'
                        .$data[amount]
                        .'از کارت به شماره'
                        .$data[senderCard]
                        .'به کارت شما به شماره'
                        .$data[recieverCard]
                        .'واریز شد'
                        EOD;
                    }
               if($data['type'] = 'withdraw') {
                        return $sendMessage=<<<EOD
                        'کاربر گرامی مبلغ مبلغ'
                        .$data[amount]
                        .'از کارت شما به شماره'
                        .$data[senderCard]
                        .'به کارت به شماره'
                        .$data[recieverCard]
                        .'واریز شد'
                        EOD;
               }
            }

            public function __set($name, $value)
            {
                $this->data[$name] = $value;
            }
        

            public function __get($name)
            {
                if (array_key_exists($name, $this->data)) {
                    return $this->data[$name];
                }

            }
       
}