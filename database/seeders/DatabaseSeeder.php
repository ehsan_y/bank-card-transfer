<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        /*$user =*/ App\Models\User::factory(10)->create();
        $users = App\Models\User::all();
        foreach($users as $user) {
            $bankAccount = \App\Models\BankAccount::create([
                        //fake bankAccount
                        'bankAccount' => '87408452'.rand(1021,4785),
                        'user_id' => $user->id,
                    ]);
            \App\Models\CardNumber::create([
                //fake cardNumber
                'cardNumber' => '610433733650'.rand(2321, 9783),
                'bankAccount_id' => $bankAccount->id,
            ]);          
        }
    }
}
